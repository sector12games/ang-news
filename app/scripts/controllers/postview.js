'use strict';

/**
 * Created by Shaun on 2/26/2015.
 */

angular.module('angNewsApp')
  .controller('PostViewCtrl', function ($scope, $routeParams, Post){
    $scope.post = Post.get($routeParams.postId);
});

