'use strict';

app.controller('AuthCtrl', function ($scope, $location, Auth, user) {
  // If the user is already logged in, redirect to the homepage
  if (user) {
    $location.path('/');
  }

  // Otherwise, log the user in using our auth firebase service
  $scope.register = function () {
    Auth.register($scope.user).then(function() {
      return Auth.login($scope.user).then(function() {
        $location.path('/');
      });
    });
  };
});
