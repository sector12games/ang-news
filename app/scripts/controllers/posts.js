'use strict';

/**
 * @ngdoc function
 * @name angNewsApp.controller:PostsCtrl
 * @description
 * # PostsCtrl
 * Controller of the angNewsApp
 */
angular.module('angNewsApp')
  .controller('PostsCtrl', function ($scope, $location, Post) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    // When first loading, retrieve all existing posts from firebase
    //$scope.posts = Post.get();
    $scope.posts = Post.all;
    $scope.post = { url: 'http://', title: ''};

    // Save the current post to firebase. Update our list of posts
    // with the newly saved data that is returned as a callback
    // from firebase.
    $scope.addPost = function()
    {
      //Post.create($scope.post).then(function (ref) {
        ////$scope.post = {url: 'http://', 'title': ''};
        //$location.path('/posts/' + ref.name());
      //});

      //Post.save($scope.post, function (ref){
      //  $scope.posts[ref.name] = $scope.post;
      //  $scope.post = { url: 'http://', title: ''};
      //});
    };

    // Delete the post from firebase, and after it is successfully
    // deleted from the server, delete it from our local list.
    //$scope.deletePost = function(postId)
    //{
      //Post.delete({id: postId}, function(){
      //  delete $scope.posts[postId];
      //});
    //};

    $scope.deletePost = function(post) {
      Post.delete(post);
    };
  });
