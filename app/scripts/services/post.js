/**
 * Created by Shaun on 2/24/2015.
 */
'use strict';

//app.factory('Post', function ($resource) {
//  return $resource('https://resplendent-heat-415.firebaseio.com/posts/:id.json');
//});

app.factory('Post', function ($firebase, FIREBASE_URL) {
  var ref = new Firebase(FIREBASE_URL);
  var posts = $firebase(ref.child('posts')).$asArray();

  var p = {
    all: posts,
    create: function (post) {
      return posts.$add(post);
    },
    get: function (postId) {
      return $firebase(ref.child('posts').child(postId)).$asObject();
    },
    delete: function (post) {
      return posts.$remove(post);
    }
  };

  return p;
});
