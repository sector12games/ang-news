/**
 * Created by Shaun on 2/26/2015.
 */
'use strict';

app.filter('hostnameFromUrl', function () {
  return function (str) {
    var url = document.createElement('a');

    url.href = str;

    return url.hostname;
  };
});
